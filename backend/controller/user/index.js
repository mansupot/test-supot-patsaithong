require('dotenv').config();
const db = require('../../sequelize-connection');

const { user } = db;
db.sequelize.sync();

const findAll = async (req, res) => {
  const users = await user.findAll();
  res.send(users);
};

module.exports = { findAll };
