require('dotenv').config();
const db = require('../../sequelize-connection');

const { news, newsReaded, user } = db;
db.sequelize.sync();

const findByUsername = async (req, res) => {
  const { userId } = req.params;
  const newsOfUser = await news.findAll({
    include: [
      {
        model: user,
        required: true,
        right: true,
        attributes: [],
        where: { userId },
      },
      {
        model: newsReaded,
        required: false,
        include: [
          {
            model: user,
            required: true,
            attributes: [],
          },
        ],
        attributes: ['news_readed_id'],
        where: { userId },
      },
    ],
    raw: true,
  });
  const newsOfProjectAll = await news.findAll({
    where: { project: 'All' },
    include: [
      {
        model: newsReaded,
        required: false,
        include: [
          {
            model: user,
            required: true,
            attributes: [],
          },
        ],
        attributes: ['news_readed_id'],
        where: { userId },
      },
    ],
    raw: true,
  });
  const listOfNews = [...newsOfProjectAll, ...newsOfUser];
  const mapRead = listOfNews.map((el) => {
    if (el['newReaded.news_readed_id']) {
      delete el['newReaded.news_readed_id'];
      return { ...el, isReaded: true };
    }
    delete el['newReaded.news_readed_id'];
    return { ...el, isReaded: false };
  });

  res.send(mapRead);
};

module.exports = { findByUsername };
