require('dotenv').config();
const { genarateToken } = require('../../jwt.config');
const db = require('../../sequelize-connection');

const { user } = db;
db.sequelize.sync();

const signIn = async (req, res) => {
  const { username, password } = req.body;
  const userResult = await user.findAll({
    where: { username, password },
  });
  const userObject = userResult[0].dataValues;
  const token = genarateToken(userObject);
  const response = {
    ...userObject,
    accessToken: token,
  };
  res.send(response);
};

module.exports = { signIn };
