const express = require('express');
const newsController = require('../../controller/news');
const { authenticateAccessToken } = require('../../jwt.config');

const router = express.Router();

router.get('/:userId', authenticateAccessToken, newsController.findByUsername);

module.exports = router;
