const express = require('express');
const userController = require('../../controller/user');
const { authenticateAccessToken } = require('../../jwt.config');

const router = express.Router();

router.get('/all', authenticateAccessToken, userController.findAll);

module.exports = router;
