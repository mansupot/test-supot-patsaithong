require('dotenv').config();
const jwt = require('jsonwebtoken');

const authenticateAccessToken = async (req, res, next) => {
  const authHeader = req.headers['authorization'];
  const token =
    authHeader && authHeader.startsWith('Bearer ') && authHeader.split(' ')[1];
  if (!token) return res.send({ msg: 'Unauthorize' });
  jwt.verify(token, process.env.SECRET_KEY, (err, value) => {
    if (err) {
      return res.send({ msg: 'Forbidden' });
    }
    next();
  });
};

const genarateToken = (userObject) => {
  return jwt.sign(userObject, process.env.SECRET_KEY, {
    expiresIn: '3h',
  });
};

module.exports = { authenticateAccessToken, genarateToken };
