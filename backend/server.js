require('dotenv').config();
const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const db = require('./sequelize-connection');

const authRouter = require('./routers/auth');
const userRouter = require('./routers/user');
const newsRouter = require('./routers/news');

const app = express();

app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
);

app.use(cors());

app.use('/auth', authRouter);
app.use('/user', userRouter);
app.use('/news', newsRouter);

app.listen(3000, async () => {
  console.log('Start server at port 3000.');
  try {
    await db.sequelize.authenticate();
    console.log('Connection has been established successfully.');
  } catch (error) {
    console.error('Unable to connect to the database:', error);
  }
});
