module.exports = (sequelize, DataTypes) => {
  const user = sequelize.define(
    'user',
    {
      userId: {
        type: DataTypes.INTEGER,
        field: 'user_id',
        primaryKey: true,
      },
      username: {
        type: DataTypes.STRING,
        field: 'user_name',
      },
      firstName: {
        type: DataTypes.STRING,
        field: 'name',
      },
      lastName: {
        type: DataTypes.STRING,
        field: 'last_name',
      },
      phoneNo: {
        type: DataTypes.STRING,
        field: 'phone_no',
      },
      project: {
        type: DataTypes.STRING,
        field: 'project',
      },
    },
    {
      tableName: 'user_data',
      timestamps: false,
      freezetablename: true,
    }
  );
  return user;
};
