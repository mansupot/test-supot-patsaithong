module.exports = (sequelize, DataTypes) => {
  const news = sequelize.define(
    'news',
    {
      newsId: {
        type: DataTypes.INTEGER,
        field: 'news_id',
        primaryKey: true,
      },
      titleTH: {
        type: DataTypes.STRING,
        field: 'title_th',
      },
      titleEN: {
        type: DataTypes.STRING,
        field: 'title_en',
      },
      detailTH: {
        type: DataTypes.STRING,
        field: 'detail_th',
      },
      detailEN: {
        type: DataTypes.STRING,
        field: 'detail_en',
      },
      project: {
        type: DataTypes.STRING,
        field: 'project',
      },
      startDate: {
        type: DataTypes.DATE,
        field: 'start_date',
      },
      stopDate: {
        type: DataTypes.DATE,
        field: 'stop_date',
      },
    },
    {
      tableName: 'news_data',
      timestamps: false,
      freezetablename: true,
    }
  );

  return news;
};
