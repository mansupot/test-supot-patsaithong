module.exports = (sequelize, DataTypes) => {
  const newsReaded = sequelize.define(
    'newReaded',
    {
      newsReadedId: {
        type: DataTypes.INTEGER,
        field: 'news_readed_id',
        primaryKey: true,
      },
      newsId: {
        type: DataTypes.INTEGER,
        field: 'news_id',
      },
      userId: {
        type: DataTypes.INTEGER,
        field: 'user_id',
      },
    },
    {
      tableName: 'news_readed',
      timestamps: false,
      freezetablename: true,
    }
  );
  return newsReaded;
};
