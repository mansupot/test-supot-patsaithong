require('dotenv').config();
const { Sequelize } = require('sequelize');

const sequelize = new Sequelize(
  process.env.DB_DATABASE,
  process.env.DB_USERNAME,
  process.env.DB_PASSWORD,
  {
    host: process.env.DB_HOST,
    dialect: 'mariadb',
    dialectOptions: { connectTimeout: 1000 },
  }
);

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.user = require('./models/user.model')(sequelize, Sequelize);
db.news = require('./models/news.model')(sequelize, Sequelize);
db.newsReaded = require('./models/newsReaded.model')(sequelize, Sequelize);

db.news.hasMany(db.user, {
  foreignKey: 'project',
  sourceKey: 'project',
});
db.user.belongsTo(db.news, {
  foreignKey: 'project',
  sourceKey: 'project',
});

db.news.hasOne(db.newsReaded, {
  foreignKey: 'news_id',
});
db.newsReaded.hasOne(db.user, {
  foreignKey: 'user_id',
});

module.exports = db;
