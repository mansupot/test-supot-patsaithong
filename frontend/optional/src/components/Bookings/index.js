import React from 'react';
import styled from 'styled-components';
import { getBookingThisWeek, getBookingToday } from './behavior';

const Container = styled.div`
  position: absolute;
  width: 1671px;
  height: 1253px;
  left: 0px;
  top: 0px;
  background: radial-gradient(
      75.44% 75.44% at 30.03% 0%,
      rgba(255, 255, 255, 0.2) 0%,
      rgba(255, 255, 255, 0) 100%
    ),
    linear-gradient(236.58deg, #bcbfc8 10.31%, #91a2c6 90.59%), #b9bdc8;
`;

const Wrapper = styled.div`
  position: absolute;
  width: 1479px;
  height: 1004px;
  left: 96px;
  top: 123px;

  background: #efeeec;
`;

const Upcomming = styled.div`
  position: absolute;
  width: 585px;
  height: 1004px;

  background: #46529d;
`;

const WrapperRoomId = styled.div`
  position: absolute;
  width: 495px;
  height: 135px;
  left: 90px;

  background: #2ebaee;
  box-shadow: 0px 4px 32px rgba(57, 64, 111, 0.25);
`;

const RoomId = styled.div`
  position: absolute;
  width: 130px;
  height: 54px;
  left: 48px;
  top: 56px;

  font-family: Roboto;
  font-style: normal;
  font-weight: bold;
  font-size: 54px;
  line-height: 100%;

  color: #ffffff;
`;

const TitleText = styled.div`
  position: absolute;
  width: 82px;
  height: 18px;
  left: 97px;
  top: 260px;

  font-family: Lato;
  font-style: normal;
  font-weight: bold;
  font-size: 18px;
  line-height: 100%;

  color: #ffffff;
`;

const Date = styled.div`
  position: absolute;
  width: 226px;
  height: 64px;
  left: ${(props) => props.left};
  top: ${(props) => props.top};

  font-family: Lato;
  font-style: normal;
  font-weight: 300;
  font-size: 64px;
  line-height: 100%;

  color: #ffffff;

  opacity: ${(props) => props.opacity || 1};
`;

const SubWrapper = styled.div`
  position: absolute;
  width: 585px;
  height: 375px;
  top: 629px;

  background: #4d59a1;
`;
const WrapperUpcomming = styled.div``;

const BetweenDate = styled.div`
  position: absolute;
  height: 16px;
  left: 92px;
  top: ${(props) => props.top};

  font-family: Roboto;
  font-style: normal;
  font-weight: normal;
  font-size: 16px;
  line-height: 100%;
  z-index: 1;

  color: #ffffff;

  opacity: 0.5;
`;
const BookingTitle = styled.div`
  position: absolute;
  height: 20px;
  left: 94px;
  top: ${(props) => props.top};

  font-family: Roboto;
  font-style: normal;
  font-weight: normal;
  font-size: 20px;
  line-height: 100%;
  z-index: 1;

  color: #ffffff;
`;

const Dashboard = styled.div`
  position: absolute;
  width: 893px;
  height: 869px;
  left: 586px;
  top: 135px;

  background: #ffffff;
  box-shadow: 0px 0px 30px rgba(0, 0, 0, 0.14);
`;

const WrapperTab = styled.div``;
const TabTitle = styled.div`
  position: absolute;
  height: 24px;
  left: ${(props) => props.left};
  top: 83px;

  font-family: Roboto;
  font-style: normal;
  font-weight: normal;
  font-size: 24px;
  line-height: 100%;
  z-index: 1;

  font-variant: small-caps;

  color: #000000;

  opacity: ${(props) => props.opacity};
`;
const Hightlight = styled.div`
  position: absolute;
  width: 51px;
  height: 4px;
  left: 673px;
  top: 131px;

  background: #707fdd;
  box-shadow: 0px -1px 16px rgba(112, 127, 221, 0.75);
  z-index: 1;
`;

const FrameDate = styled.div`
  position: absolute;
  width: 893px;
  height: 47px;
  top: ${(props) => props.top};

  background: #f7f7f7;
  border: 1px solid #ececec;
  box-sizing: border-box;
  z-index: 1;
`;

const Line = styled.div`
  position: absolute;
  width: 868px;
  height: 0px;
  left: -376px;
  top: 434px;

  border: 1px solid #ececec;
  transform: rotate(90deg);
`;

const ShowDate = styled.div`
  position: absolute;
  height: 18px;
  left: 94px;
  top: 15px;

  font-family: Roboto;
  font-style: normal;
  font-weight: bold;
  font-size: 18px;
  line-height: 100%;

  color: #787878;
`;

const Point = styled.div`
  position: absolute;
  width: 10px;
  height: 10px;
  left: 54px;
  top: ${(props) => props.top};
  border-radius: 10px;
  background: #3dc7d2;
`;

const BetweenDateDashboard = styled.div`
  position: absolute;
  height: 16px;
  left: 92px;
  top: ${(props) => props.top};

  font-family: Roboto;
  font-style: normal;
  font-weight: normal;
  font-size: 16px;
  line-height: 100%;

  color: #000000;

  opacity: 0.5;
`;
const BookingTitleDashboard = styled.div`
  position: absolute;
  height: 20px;
  left: 94px;
  top: ${(props) => props.top};

  font-family: Roboto;
  font-style: normal;
  font-weight: normal;
  font-size: 20px;
  line-height: 100%;

  color: #000000;
`;

function Bookings({ location }) {
  const roomId = new URLSearchParams(location.search).get('roomId');

  const [bookingToday, setBookingToday] = React.useState([]);
  const [bookingThisTommorrow, setBookingThisTommorrow] = React.useState([]);

  React.useEffect(() => {
    setBookingToday(getBookingToday({ roomId, day: '2019-09-28' }));
    setBookingThisTommorrow(getBookingToday({ roomId, day: '2019-09-29' }));
  }, []);

  const handleConverToTimeOnly = (dateTime) => {
    const time = dateTime.split(' ')[1];
    const hh = time.split(':')[0];
    const mm = time.split(':')[1];
    return hh + ':' + mm;
  };

  return (
    <div>
      <Container>
        <Wrapper>
          <Upcomming>
            <WrapperRoomId>
              <RoomId>{roomId}</RoomId>
            </WrapperRoomId>
            <TitleText>Upcomming</TitleText>
            <Date left="90px" top="336px" opacity="0.5">
              Monday
            </Date>
            <Date left="90px" top="415px">
              28 Sep
            </Date>
            <WrapperUpcomming>
              {bookingToday.map(({ startTime, endTime, title }, i) => {
                const startTimeOnly = handleConverToTimeOnly(startTime);
                const endTimeOnly = handleConverToTimeOnly(endTime);
                return (
                  <React.Fragment key={i}>
                    <BetweenDate top={`${569 + 66 * i}px`}>
                      {startTimeOnly} - {endTimeOnly}
                    </BetweenDate>
                    <BookingTitle top={`${593 + 66 * i}px`}>
                      {title}
                    </BookingTitle>
                  </React.Fragment>
                );
              })}
            </WrapperUpcomming>
            <SubWrapper />
          </Upcomming>
          <WrapperTab>
            <TabTitle left="649px" opacity="1">
              THIS WEEK
            </TabTitle>
            <TabTitle left="830px" opacity="0.5">
              NEXT WEEK
            </TabTitle>
            <TabTitle left="1018px" opacity="0.5">
              WHOLE MONTH
            </TabTitle>
            <Hightlight />
          </WrapperTab>
          <Dashboard>
            <Line />
            <FrameDate top="78px">
              <ShowDate>Today (Mon, 28 Sep)</ShowDate>
            </FrameDate>
            {bookingToday.map(({ startTime, endTime, title }, i) => {
              const startTimeOnly = handleConverToTimeOnly(startTime);
              const endTimeOnly = handleConverToTimeOnly(endTime);
              return (
                <div>
                  <Point top={`${178 + 66 * i}px`} />
                  <BetweenDateDashboard top={`${178 + 66 * i}px`}>
                    {startTimeOnly} - {endTimeOnly}
                  </BetweenDateDashboard>
                  <BookingTitleDashboard top={`${199 + 66 * i}px`}>
                    {title}
                  </BookingTitleDashboard>
                </div>
              );
            })}
            <FrameDate top="557px">
              <ShowDate>Tomorrow (Tue, 29 Sep)</ShowDate>
            </FrameDate>
            {bookingThisTommorrow.map(({ startTime, endTime, title }, i) => {
              const startTimeOnly = handleConverToTimeOnly(startTime);
              const endTimeOnly = handleConverToTimeOnly(endTime);
              return (
                <div>
                  <Point top={`${665 + 66 * i}px`} />
                  <BetweenDateDashboard top={`${665 + 66 * i}px`}>
                    {startTimeOnly} - {endTimeOnly}
                  </BetweenDateDashboard>
                  <BookingTitleDashboard top={`${689 + 66 * i}px`}>
                    {title}
                  </BookingTitleDashboard>
                </div>
              );
            })}
          </Dashboard>
        </Wrapper>
      </Container>
    </div>
  );
}

export default Bookings;
