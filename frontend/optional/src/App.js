import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import Bookings from './components/Bookings';

function App() {
  return (
    <BrowserRouter>
      <Switch>
        <Route path="/bookinngs/today" component={Bookings} />
      </Switch>
    </BrowserRouter>
  );
}

export default App;
