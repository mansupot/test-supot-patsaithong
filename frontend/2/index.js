const bookingData = require('./demo-booking-data.json');

//utility function
const isValidBetweenDate = (from, to, search) => {
  const fromTime = Date.parse(from);
  const toTime = Date.parse(to);
  const searchTime = Date.parse(search);
  const withInRange = searchTime >= fromTime && searchTime <= toTime;
  return withInRange;
};
//---------------------------------------------------------

//2.1
const checkAvailability = (roomId, startTime, endTime) => {
  if (Date.parse(endTime) < Date.parse(startTime)) return false;
  const isAvailable = bookingData.some((el) => {
    const startDateCheck = isValidBetweenDate(
      el.startTime,
      el.endTime,
      startTime
    );
    const endDateCheck = isValidBetweenDate(el.startTime, el.endTime, endTime);
    return roomId === el.roomId && (startDateCheck || endDateCheck);
  });
  return !isAvailable;
};

const isAvailable = checkAvailability(
  'A101',
  '2019-09-01 13:00:00',
  '2019-09-02 14:00:00'
);

console.log('2.1 =================================================');
console.log('isAvailable => ', isAvailable);
//---------------------------------------------------------

//2.2

const currentDay = '2019-09-28';

const getBookingToday = ({ roomId }) => {
  const startOfDay = new Date(currentDay);
  const endOfDay = new Date(currentDay);
  endOfDay.setHours(23, 59, 59, 999);
  return bookingData.filter((el) => {
    const validStartTime = isValidBetweenDate(
      startOfDay,
      endOfDay,
      el.startTime
    );
    return el.roomId === roomId && validStartTime;
  });
};

const getBookingThisWeek = ({ roomId }) => {
  const currentDate = new Date(currentDay);
  let firstDateOfWeek;
  let lastDateOfWeek;

  const firstDayOfWeek = currentDate.getDate() - currentDate.getDay();
  const lastDayOfWeek = firstDayOfWeek + 7;
  firstDateOfWeek = new Date(currentDate.setDate(firstDayOfWeek));
  lastDateOfWeek = new Date(currentDate.setDate(lastDayOfWeek));

  return bookingData.filter((booking) => {
    const validStartTime = isValidBetweenDate(
      firstDateOfWeek,
      lastDateOfWeek,
      booking.startTime
    );
    return booking.roomId === roomId && validStartTime;
  });
};

const getBookingNextWeek = ({ roomId }) => {
  const getNextWeek = (value) => {
    const date = new Date(value);
    const today = date.getDate();
    const dayOfTheWeek = date.getDay();
    const startDate = new Date(date.setDate(today - dayOfTheWeek + 7));
    const endDate = new Date(date.setDate(today - dayOfTheWeek + 13));
    return {
      startDate,
      endDate,
    };
  };

  const currentDate = new Date(currentDay);
  let firstDateOfWeek;
  let lastDateOfWeek;

  const { startDate, endDate } = getNextWeek(currentDate);
  firstDateOfWeek = startDate;
  lastDateOfWeek = endDate;

  return bookingData.filter((booking) => {
    const validStartTime = isValidBetweenDate(
      firstDateOfWeek,
      lastDateOfWeek,
      booking.startTime
    );
    return booking.roomId === roomId && validStartTime;
  });
};

const listOfBookingToday = getBookingToday({ roomId: 'A101' });
const listOfBookingThisWeek = getBookingThisWeek({ roomId: 'A101' });
const listOfBookingNextWeek = getBookingNextWeek({ roomId: 'A101' });

console.log('2.2 =================================================');
console.log('listOfBookingToday => ', listOfBookingToday);
console.log('listOfBookingThisWeek => ', listOfBookingThisWeek);
console.log('listOfBookingNextWeek => ', listOfBookingNextWeek);
