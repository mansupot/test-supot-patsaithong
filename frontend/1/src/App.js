import React, { useState, useEffect } from 'react';
import axios from 'axios';
import './App.css';

function App() {
  const [images, setImages] = useState([]);
  useEffect(() => {
    const fetchData = async () => {
      const getImages = await axios.get('https://picsum.photos/v2/list');
      setImages(getImages.data);
    };
    fetchData();
  }, []);
  return (
    <div className="container">
      {images.map((el, i) => (
        <img className="image" src={el.download_url} alt={el.author} />
      ))}
    </div>
  );
}

export default App;
